package gridgif

import (
	"image"
	"image/color"
	"image/draw"
	"image/gif"
	"image/png"
	"log"
	"os"
	"sync"

	"github.com/andybons/gogif"
)

// Cell represents each cell on a grid
type Cell struct {
	color color.RGBA
	image *image.Image
}

// Grid represents a grid with cells
type Grid struct {
	cells [][]Cell
}

// Animation represents a sequence of Grid
type Animation struct {
	grids []*Grid
	delay []int
}

func (grid *Grid) getImage(pxCellWidth int, pxCellHeight int) *image.RGBA {
	gridImage := image.NewRGBA(image.Rect(0, 0, pxCellWidth*len(grid.cells), pxCellHeight*len(grid.cells[0])))

	// Draw cells
	for i := 0; i < len(grid.cells); i++ {
		for j := 0; j < len(grid.cells[i]); j++ {
			rect := image.Rect(i*pxCellWidth, j*pxCellHeight, i*pxCellWidth+pxCellWidth, j*pxCellHeight+pxCellHeight)

			draw.Draw(gridImage, rect, &image.Uniform{grid.cells[i][j].color}, image.ZP, draw.Src)

			if grid.cells[i][j].image != nil {
				draw.Draw(gridImage, rect, *grid.cells[i][j].image, image.ZP, draw.Over)
			}
		}
	}

	return gridImage
}

// Draw draws the grid into a file
func (grid *Grid) Draw(pxCellWidth int, pxCellHeight int, filename string) {
	gridImage := grid.getImage(pxCellWidth, pxCellHeight)

	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(file, gridImage); err != nil {
		file.Close()
		log.Fatal(err)
	}

	if err := file.Close(); err != nil {
		log.Fatal(err)
	}
}

func (animation *Animation) getImages(pxCellWidth int, pxCellHeight int) []*image.RGBA {
	images := make([]*image.RGBA, len(animation.grids))
	for i := range animation.grids {
		images[i] = animation.grids[i].getImage(pxCellWidth, pxCellHeight)
	}

	return images
}

// NewGridSingleColor creates a grid with a single color
func NewGridSingleColor(width int, height int, singleColor color.RGBA) *Grid {
	cells := make([][]Cell, width)

	for i := range cells {
		cells[i] = make([]Cell, height)

		for j := range cells[i] {
			cells[i][j].color = singleColor
		}
	}

	return &Grid{
		cells: cells,
	}
}

// NewGridAlternateColors creates a grid by alternating colors
func NewGridAlternateColors(width int, height int, firstColor color.RGBA, secondColor color.RGBA) *Grid {
	cells := make([][]Cell, width)

	for i := range cells {
		cells[i] = make([]Cell, height)

		for j := range cells[i] {

			var squareColor color.RGBA
			if (i+j%2)%2 == 0 {
				squareColor = firstColor
			} else {
				squareColor = secondColor
			}

			cells[i][j].color = squareColor
		}
	}

	return &Grid{
		cells: cells,
	}
}

// Converts an image to an image.Paletted with 256 colors.
func imageToPaletted(img image.Image) *image.Paletted {
	pm, ok := img.(*image.Paletted)
	if !ok {
		b := img.Bounds()
		pm = image.NewPaletted(b, nil)
		q := &gogif.MedianCutQuantizer{NumColor: 256}
		q.Quantize(pm, b, img, image.ZP)
	}
	return pm
}

func (animation *Animation) getPaletted(pxCellWidth int, pxCellHeight int) []*image.Paletted {
	images := animation.getImages(pxCellWidth, pxCellHeight)

	paletteds := make([]*image.Paletted, len(animation.grids))

	var wg sync.WaitGroup
	for i := range animation.grids {
		wg.Add(1)
		go func(i int) {
			paletteds[i] = imageToPaletted(images[i])
			draw.Draw(paletteds[i], paletteds[i].Rect, images[i], image.ZP, draw.Src)
			wg.Done()
		}(i)
	}

	wg.Wait()
	return paletteds
}

// Draw draws an animation as a gif
func (animation *Animation) Draw(pxCellWidth int, pxCellHeight int, filename string) {
	anim := gif.GIF{Delay: animation.delay, Image: animation.getPaletted(pxCellWidth, pxCellHeight)}

	file, err := os.Create(filename)
	if err != nil {
		log.Fatal(err)
	}

	if err := gif.EncodeAll(file, &anim); err != nil {
		file.Close()
		log.Fatal(err)
	}

	if err := file.Close(); err != nil {
		log.Fatal(err)
	}
}

// AddDuplicatedGrid appends a new grid to the animation with the same
// values as the last one.
func (animation *Animation) AddDuplicatedGrid() *Grid {
	previousGrid := animation.grids[len(animation.grids)-1]

	duplicate := make([][]Cell, len(previousGrid.cells))
	for i := range previousGrid.cells {
		duplicate[i] = make([]Cell, len(previousGrid.cells[i]))
		copy(duplicate[i], previousGrid.cells[i])
	}

	duplicatedGrid := &Grid{cells: duplicate}
	animation.grids = append(animation.grids, duplicatedGrid)
	animation.delay = append(animation.delay, animation.delay[len(animation.delay)-1])

	return duplicatedGrid
}

// NewAnimationWhite creates a new animation with a single white grid
func NewAnimationWhite(width int, height int, delay int) *Animation {
	return NewAnimation(width, height, delay, color.RGBA{255, 255, 255, 255})
}

// NewAnimation creates a new animation with a single colored grid
func NewAnimation(width int, height int, delay int, gridColor color.RGBA) *Animation {
	return &Animation{
		grids: []*Grid{NewGridSingleColor(width, height, gridColor)},
		delay: []int{delay},
	}
}

// SetCellColor sets the color of the (x, y) cell of the last grid on the animation
func (animation *Animation) SetCellColor(x int, y int, newColor color.RGBA) {
	lastGrid := animation.grids[len(animation.grids)-1]
	lastGrid.cells[x][y].color = newColor
}

// SetCellImage sets the image of the (x, y) cell of the last grid on the animation
func (animation *Animation) SetCellImage(x int, y int, filename string) {
	lastGrid := animation.grids[len(animation.grids)-1]

	infile, err := os.Open(filename)
	if err != nil {
		panic(err.Error)
	}
	defer infile.Close()

	src, _, err := image.Decode(infile)
	if err != nil {
		panic(err.Error)
	}

	lastGrid.cells[x][y].image = &src
}

// AnimateSwapCell appends a new grid with cells (x1, y1) and (x2, y2) swapped
func (animation *Animation) AnimateSwapCell(x1 int, y1 int, x2 int, y2 int) {
	newGrid := animation.AddDuplicatedGrid()

	// Swap colors
	newGrid.cells[x1][y1].color, newGrid.cells[x2][y2].color = newGrid.cells[x2][y2].color, newGrid.cells[x1][y1].color

	// Swap image
	newGrid.cells[x1][y1].image, newGrid.cells[x2][y2].image = newGrid.cells[x2][y2].image, newGrid.cells[x1][y1].image
}

// AnimateSwapCellColor appends a new grid with cells (x1, y1) and (x2, y2) colors swapped
func (animation *Animation) AnimateSwapCellColor(x1 int, y1 int, x2 int, y2 int) {
	newGrid := animation.AddDuplicatedGrid()

	// Swap colors
	newGrid.cells[x1][y1].color, newGrid.cells[x2][y2].color = newGrid.cells[x2][y2].color, newGrid.cells[x1][y1].color
}

// AnimateSwapCellImage appends a new grid with cells (x1, y1) and (x2, y2) images swapped
func (animation *Animation) AnimateSwapCellImage(x1 int, y1 int, x2 int, y2 int) {
	newGrid := animation.AddDuplicatedGrid()

	// Swap image
	newGrid.cells[x1][y1].image, newGrid.cells[x2][y2].image = newGrid.cells[x2][y2].image, newGrid.cells[x1][y1].image
}
