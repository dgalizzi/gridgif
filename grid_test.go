package gridgif

import (
	"image/color"
	"testing"
)

func TestDraw(t *testing.T) {
	cellsX := 8
	cellsY := 16

	darkSquareColor := color.RGBA{140, 162, 173, 255}
	lightSquareColor := color.RGBA{222, 227, 230, 255}

	grid := NewGridAlternateColors(cellsX, cellsY, lightSquareColor, darkSquareColor)
	grid.Draw(64, 32, "grid.png")

	animation := Animation{
		grids: []*Grid{grid},
		delay: []int{50},
	}

	grid2 := animation.AddDuplicatedGrid()
	grid2.cells[0][0].color = color.RGBA{255, 0, 0, 255}
	grid2.cells[cellsX-1][cellsY-1].color = color.RGBA{0, 255, 0, 255}
	grid2.Draw(64, 32, "grid2.png")

	animation.Draw(64, 32, "grid.gif")
}

func TestDrawMovingCell(t *testing.T) {
	cellsX := 4
	cellsY := 3

	animation := NewAnimation(cellsX, cellsY, 15, color.RGBA{20, 60, 80, 255})
	animation.SetCellColor(0, 0, color.RGBA{255, 160, 0, 255})

	for y := 0; y < cellsY; y++ {
		for x := 1; x < cellsX; x++ {
			animation.AnimateSwapCell(x, y, x-1, y)
		}

		if y+1 < cellsY {
			animation.AnimateSwapCell(cellsX-1, y, 0, y+1)
		}
	}

	animation.Draw(32, 32, "movingCellAnimation.gif")
}

func TestDrawMovingImage(t *testing.T) {
	cellsX := 8
	cellsY := 8

	animation := NewAnimationWhite(cellsX, cellsY, 15)

	darkSquareColor := color.RGBA{140, 162, 173, 255}
	lightSquareColor := color.RGBA{222, 227, 230, 255}

	animation.grids[0] = NewGridAlternateColors(cellsX, cellsY, lightSquareColor, darkSquareColor)
	animation.SetCellImage(0, 0, "testAssets/wn.png")

	for y := 0; y < cellsY; y++ {
		for x := 1; x < cellsX; x++ {
			animation.AnimateSwapCellImage(x, y, x-1, y)
		}

		if y+1 < cellsY {
			animation.AnimateSwapCellImage(cellsX-1, y, 0, y+1)
		}
	}

	animation.Draw(80, 80, "movingKnightAnimation.gif")
}
